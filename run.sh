#!/bin/bash
echo 'Launching container...'
docker run -ti --rm \
		-v $(PWD):/usr/src/app \
		-p 8080:8080 \
		neo-node bash