$(function() {
    $('#geolocateBtn').click(function() {
        findMyLocation();
    });

    $('#findATMsBtn').click(function(e) {
        hideAlert();
        var coordinatesInputVal = $('#coordinatesInput').val();
        if (coordinatesInputVal) {
            var parsedCoordinates = JSON.parse(coordinatesInputVal);
            console.log(parsedCoordinates);
            if (parsedCoordinates[0] && parsedCoordinates[1]) {
                var pos = {
                    lat: parsedCoordinates[0],
                    lng: parsedCoordinates[1]
                };
                nearestATMs(pos);
            } else {
                showAlert("Invalid coordinates. They must be in the form [<latitude>, <longitude>].", "warning");
            }

        } else {
            showAlert("Please input some coordinates", "warning");
        }

    });
});

function showAlert(text, alertClass) {
    var alertElem = $('#alert');
    alertElem.removeClass(alertElem.attr('class'));
    alertElem.addClass("alert alert-" + alertClass);
    $('#alert .alert-text').text(text);
    alertElem.fadeIn();
}

function hideAlert() {
    var alertElem = $('#alert');
    alertElem.fadeOut();
}

function addPlacesAutoComplete() {
    var placeNameInput = $('#placeNameInput')[0];
    var options = {
        types: ['geocode'],
        componentRestrictions: {
            country: 'uk'
        }
    };
    autocomplete = new google.maps.places.Autocomplete(placeNameInput, options);
    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    place = autocomplete.getPlace();
    console.log(place);
    var coordinatesField = place.geometry.location;
    var coordinatesObj = {
        lat: coordinatesField.lat(),
        lng: coordinatesField.lng()
    }
    setCoordinatesField(coordinatesObj);
}

var map, infoWindow, markers;
var markerPath = "http://maps.google.com/mapfiles/ms/icons/";

function initMap() {
    addPlacesAutoComplete();
    var ukCentre = {
        lat: 54.00366,
        lng: -2.547855
    };
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: ukCentre
    });
    var marker = new google.maps.Marker({
        position: ukCentre,
        map: map,
        icon: markerPath + "blue-dot.png"
    });
}

function findMyLocation() {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            setCoordinatesField(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    var errorMessage = browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.';
    infoWindow.setContent(errorMessage);
    infoWindow.open(map);
    showAlert("Please input a location", "error");
}

function nearestATMs(pos) {
    var facilitiesArr = new Array();
    $('input[name="facilities"]:checked').each(function() {
        var facilityVal = this.value;
        facilitiesArr.push(this.value);
    });
    var banksArr = new Array();
    $('input[name="banks"]:checked').each(function() {
        var bankVal = this.value;
        banksArr.push(this.value);
    });
    $.get("/api/nearest-atms", {
            latitude: pos.lat,
            longitude: pos.lng,
            facilities: facilitiesArr,
            banks: banksArr,
            limit: 10
        })
        .done(function(atms) {
            markers = [];
            overlappingMarkers = {};
            var content = "<strong>Me</strong>";
            var myPosMarker = createMarker(pos, content, "ltblu-pushpin.png");
            markers.push(myPosMarker);
            console.log(atms);
            atms.forEach(function(atm) {
                plotATM(atm);
            });
            Object.keys(overlappingMarkers)
                .map(function(key) {
                    return overlappingMarkers[key];
                })
                .forEach(function(marker) {
                    var pos = marker.pos;
                    var infowindowText = "";
                    // merge all infowindow texts into one
                    marker.contents.forEach(function(content) {
                        infowindowText += content + "<br />";
                    });
                    var marker = createMarker(pos, infowindowText, "dollar.png");
                    markers.push(marker);
                });
            zoomToFit();

        })
        .fail(function(error) {
            showAlert("Request timeout or is in error " + error, "error");
        });
}


function plotATM(atm) {
    var atmPosObj = atm.GeographicLocation;
    var pos = {
        lat: Number(atmPosObj.Latitude),
        lng: Number(atmPosObj.Longitude)
    };
    var bankName = atm.Organisation.ParentOrganisation.OrganisationName.LegalName;
    var ATMServices = atm.ATMServices;
    var AdditionalATMServices = atm.AdditionalATMServices;
    var distanceInKm = (atm.distance / 1000).toFixed(3) + " km";
    var content = "<strong>" + bankName + "</strong><br />";
    content += distanceInKm + " away <br />";
    if (ATMServices) {
        content += ATMServices.join(" ") + "<br />";
    }
    if (AdditionalATMServices) {
        content += AdditionalATMServices.join(" ") + "<br />";
    }

    if (!overlappingMarkers.hasOwnProperty(distanceInKm)) { // if the distance hasn't been plotted yet
        overlappingMarkers[distanceInKm] = {
            pos: pos,
            contents: []
        }; // init position and infowindow text
    }
    overlappingMarkers[distanceInKm].contents.push(content); // append the overlapping infowindow text for the same marker
}


function zoomToFit() {
    var bounds = new google.maps.LatLngBounds();
    markers.forEach(function(marker) {
        if (marker.getVisible()) {
            bounds.extend(marker.getPosition());
        }
    });
    map.fitBounds(bounds);

}

function setCoordinatesField(pos) {
    if (pos.hasOwnProperty('lat') && pos.hasOwnProperty('lng')) {
        var coordinatesString = "[" + pos.lat + ", " + pos.lng + "]";
        $('#coordinatesInput').val(coordinatesString);
    } else {
        showAlert("Invalid coordinates. The place doesn't have a coordinates field.", "error");
    }

}

function createMarker(pos, content, icon) {
    icon = markerPath + (icon ? icon : "pink-dot.png");
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        icon: icon
    });
    var infoWindow = new google.maps.InfoWindow;
    infoWindow.setPosition(pos);
    infoWindow.setContent(content);

    google.maps.event.addListener(marker, 'click', function() {
        var markerContent = content;
        infoWindow.setContent(markerContent);
        infoWindow.open(map, this);
    });
    return marker;
}