var request = require("request-promise");
var monk = require('monk');
var wrap = require('co-monk');
var db = monk('mongodb/local');
//var atms = wrap(db.get('atms'));

/**
 * All bank names and how many ATMs they have
 * GET /api/bank-names
 * @returns JSON of bank names and how many ATMs they have
 **/
exports.bankNames = function*() {
    this.type = 'json';
    var banks = yield allATMs()
        .then((banks) => {
            return banks.reduce((acc, bank) => {
                var bankName = bank.Organisation.ParentOrganisation.OrganisationName.LegalName;
                if (!acc.hasOwnProperty(bankName)) {
                    acc[bankName] = {};
                    acc[bankName].BankName = bankName;
                    acc[bankName].ATMCount = 0;
                }
                acc[bankName].ATMCount++;
                return acc;
            }, {});
        });
    this.body = banks;
}

/**
 * All bank facilities and how many ATMs have them
 * GET /api/bank-facilities
 * @returns JSON of bank facilities and how many ATMs have them
 **/
exports.bankFacilities = function*() {
    this.type = 'json';
    var banks = yield allATMs()
        .then((banks) => {
            return banks.reduce((acc, bank) => {
                var facilitiesArr = bank.ATMServices;
                var additionalFacilitiesArr = bank.ATMServices;
                facilitiesArr.forEach(function(facilityName) {
                    if (!acc.hasOwnProperty(facilityName)) {
                        acc[facilityName] = {};
                        acc[facilityName].FacilityName = facilityName;
                        acc[facilityName].ATMCount = 0;
                    }
                    acc[facilityName].ATMCount++;
                });
                additionalFacilitiesArr.forEach(function(facilityName) {
                    if (!acc.hasOwnProperty(facilityName)) {
                        acc[facilityName] = {};
                        acc[facilityName].FacilityName = facilityName;
                        acc[facilityName].ATMCount = 0;
                    }
                    acc[facilityName].ATMCount++;
                });
                return acc;
            }, {});
        });
    this.body = banks;
}

/**
 * GET /api/all
 * All ATMs in full Open Banking API ATM form
 * @param limit (optional) Number limit number of results to limit to
 * @returns Whole list of banks in the full Open Banking API ATM form, truncated by limit if provided
 **/
exports.all = function*() {
    this.type = 'json';
    var limit = this.request.query.limit;
    var atms = yield allATMs();
    if (limit) {
        atms = atms.slice(0, limit);
    }
    this.body = atms;
};

/**
 * GET /api/nearest-atms
 * Nearest ATMs to a given {Latitude, Longitude}
 * @param latitude Number Latitude in degrees
 * @param longitude Number Longitude in degrees
 * @param facilities Array Facilities desired
 * @param banks Array Bank names required (word matching i.e. bthe bank company name specified can be found in the bank's legalname field)
 * @param limit Number limit number of results to limit to
 * @returns Whole list of banks in the full Open Banking API ATM form
 **/
exports.nearestATMs = function*() {
    var latitude = this.request.query.latitude;
    var longitude = this.request.query.longitude;
    var limit = this.request.query.limit;
    var facilities = this.request.query['facilities[]'] ? this.request.query['facilities[]'] : new Array();
    var banks = this.request.query['banks[]'] ? this.request.query['banks[]'] : new Array();
    if (typeof facilities === 'string') {
        var facilities = [facilities]; // wrap in an array
    }
    if (typeof banks === 'string') {
        var banks = [banks]; // wrap in an array
    }
    if (latitude && longitude) {
        this.type = 'json';
        var latLonObj = {
            'Latitude': latitude,
            'Longitude': longitude
        };
        var atms = yield allATMs()
            .then((atms) => {
                return atms.map((atm) => {
                        var atmLatLonObj = atm.GeographicLocation;
                        var distance = calculateDistance(latLonObj, atmLatLonObj);
                        atm.distance = distance;
                        return atm;
                    })
                    .sort(compareDistance);
            });
        if (facilities.length) {
            atms = atms.filter((atm) => filterATMFacilities(atm, facilities));
        }
        if (banks.length) {
            atms = atms.filter((atm) => filterATMBanks(atm, banks));
        }
        if (limit) {
            atms = atms.slice(0, limit);
        }
        this.body = atms;
    } else {
        this.response.status = 400;
        this.response.body = "Latitude and longitude must be specified using ?latitude=<lat>&longitude=<lon>&limit=<limit>. You provided " + JSON.stringify(this.request.query);
    }

};

/**
 * Gets all ATMs from MongoDB using Monk.
 * @param fields Array array containing field names | default: [] (all)
 * @param limit of rows to retrieve | default: null (all results)
 * @returns Promise containing atms with selected fields up to limit limit
 **/
function allATMs(fields = [], limit = null) {
    //fields = ['SiteName', 'ATMID', 'Organisation.ParentOrganisation.OrganisationName.LegalName'];
    return db.get('atms').find({}, {
        limit: limit,
        fields: fields
    });
}

/**
 * @param atm Object The ATM object
 * @param desiredFacilities Array[string] containing desired facilities
 * @return boolean whether the atm has all desiredFacilities
 **/
function filterATMFacilities(atm, desiredFacilities) {
    var bankFacilitiesArr = atm.ATMServices ? atm.ATMServices : [];
    var bankAdditionalFacilitiesArr = atm.AdditionalATMServices ? atm.AdditionalATMServices : [];
    var hasDesiredFacilities = desiredFacilities.every((facility) => {
        var hasFacility = (bankFacilitiesArr.includes(facility) || bankAdditionalFacilitiesArr.includes(facility));
        return hasFacility;
    });
    return hasDesiredFacilities;
}

/**
 * @param atm Object The ATM object
 * @param bankNames Array[string] containing the bank names to filter on
 * @return boolean whether the atm has all bankNames
 **/
function filterATMBanks(atm, bankNames) {
    var atmBankName = atm.Organisation.ParentOrganisation.OrganisationName.LegalName ? atm.Organisation.ParentOrganisation.OrganisationName.LegalName : '';
    var matchesBankName = bankNames.find(bankName => {
        var bankCompany = bankName.split("_").join(" ").toLowerCase();
        atmBankName = atmBankName.toLowerCase();
        return atmBankName.includes(bankCompany); // full trading name of bank contains the company name
    });
    return matchesBankName;
}

/**
 * Calculates distance between two latLon objects in the form {Latitude, Longitude}
 * http://www.movable-type.co.uk/scripts/latlong.html
 * @returns Number distance in metres
 **/
function calculateDistance(latLonA, latLonB) {
    var lat1 = Number(latLonA.Latitude),
        lat2 = Number(latLonB.Latitude);
    var lon1 = Number(latLonA.Longitude),
        lon2 = Number(latLonB.Longitude);
    var φ1 = lat1.toRadians(),
        φ2 = lat2.toRadians(),
        Δλ = (lon2 - lon1).toRadians(),
        R = 6371e3; // gives d in metres
    var d = Math.acos(Math.sin(φ1) * Math.sin(φ2) + Math.cos(φ1) * Math.cos(φ2) * Math.cos(Δλ)) * R;
    return d;
}

/**
 * Extends Number prototype to add toRadians() conversion
 **/
Number.prototype.toRadians = function() {
    return this * (Math.PI / 180);
}

/**
 * Distance comparator function
 **/
function compareDistance(atm1, atm2) {
    var atm1Dist = atm1.distance ? atm1.distance : 0;
    var atm2Dist = atm2.distance ? atm2.distance : 0;
    var difference = atm1Dist - atm2Dist;
    return difference;
}