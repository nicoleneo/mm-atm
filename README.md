# ATM Finder
Finds ATMs by utilising data from the Open Banking ATMs API. Written in Node.js using the Koa framework (first Node.js framework I've ever used!) and MongoDB to store the database.

The GUI gives the user the ability to find ATMs by typing in an address or by detecting their location using the Google Maps/Places API.

## Running
Docker Compose is used to stitch together a Node.js and a MongoDB container and make them aware of each other.

Done in Docker Compose as my neo.my hosting package is of the Dummies Edition LAMP CPanel type (well it's cheap!).

```bash
$ docker-compose up
```

Visit [http://localhost:3000](http://localhost:3000) for the GUI to search for the nearest ATMs.

## Live
Now hosted on [http://mm-atm.neo.my](http://mm-atm.neo.my) on a CentOS 7 VPS with Nginx and Docker!!

## Importing data
The Open Banking ATMs API data was downloaded using Postman to pretty format them. The meta field was removed, and the data field was extracted and moved to the top level as an array. They were imported into the 'atms' collection in MongoDB.

**This has already been done and persisted in the .db folder.** In case of file corruption, pull from the repo.

```bash
docker-compose exec mongodb bash
$ cd /data/db
$ cd open-banking-json
$ mongoimport --db local --collection atms --jsonArray --file <fileName>.json 
```

## Endpoints
Endpoints are under the `/api` route i.e. `http://localhost:3000/api`

1. `GET /api/nearest-atms`
    - `latitude` (required) Number Latitude in degrees
    - `longitude` (required) Number Longitude in degrees
    - `facilities` (optional) Array Facilities required
    - `banks` (optional) Array Bank names required (word matching i.e. the bank company name specified can be found in the bank's legalname field)
    - `limit` (optional) Number limit number of results to limit to

Gets all ATMs, calculates their distance to the coordinates and sorts them, returning the top `limit` if specified.

```json
[{
    "_id": "59baea92b8b7a8b83a14976d",
    "Organisation": {
        "ParentOrganisation": {
            "LEI": "549300PPXHEU2JF0AM85",
            "OrganisationName": {
                "LegalName": "LLOYDS BANKING GROUP PLC"
            }
        },
        "Brand": {
            "TrademarkIPOCode": "UK",
            "TrademarkID": "UK00001286075"
        }
    },
    "BranchIdentification": "11004599",
    "ATMID": "HA94AC11",
    "LocationCategory": "FactoryOrOffice",
    "SiteID": "11004531",
    "Address": {
        "StreetName": "HARBOURSIDE",
        "BuildingNumberOrName": "REMOTE - OTHER",
        "PostCode": "BS1 5LF",
        "OptionalAddressField": "HARBOURSIDE; 10 CANONS WAY",
        "TownName": "BRISTOL",
        "CountrySubDivision": "AVON",
        "Country": "GB"
    },
    "GeographicLocation": {
        "Latitude": "51.448601",
        "Longitude": "-2.603023"
    },
    "AccessibilityTypes": ["AudioCashMachine", "WheelchairAccess"],
    "SupportedLanguages": ["eng", "spa", "ger", "fre"],
    "ATMServices": ["CashWithdrawal", "PINChange", "MobilePhoneTopUp", "Balance", "FastCash"],
    "AdditionalATMServices": ["PINUnblock"],
    "Currency": ["GBP"],
    "MinimumValueDispensed": "£10",
    "distance": 133.49479429331518
}, {
    "_id": "59baea9cb8b7a8b83a14a743",
    "Organisation": {
        "ParentOrganisation": {
            "LEI": "549300PPXHEU2JF0AM85",
            "OrganisationName": {
                "LegalName": "LLOYDS BANKING GROUP PLC"
            }
        },
        "Brand": {
            "TrademarkIPOCode": "UK",
            "TrademarkID": "UK00001286876"
        }
    },
    "BranchIdentification": "30000199",
    "ATMID": "L59AAC11",
    "LocationCategory": "RemoteUnit",
    "SiteID": "30000131",
    "Address": {
        "StreetName": "CANONS HOUSE",
        "BuildingNumberOrName": "REMOTE - OTHER",
        "PostCode": "BS1 5LL",
        "OptionalAddressField": "CANONS HOUSE; CANONS WAY",
        "TownName": "BRISTOL",
        "CountrySubDivision": "AVON",
        "Country": "GB"
    },
    "GeographicLocation": {
        "Latitude": "51.448621",
        "Longitude": "-2.60085"
    },
    "AccessibilityTypes": ["AudioCashMachine", "WheelchairAccess"],
    "SupportedLanguages": ["eng", "spa", "ger", "fre"],
    "ATMServices": ["CashWithdrawal", "PINChange", "MobilePhoneTopUp", "Balance", "FastCash"],
    "AdditionalATMServices": ["PINUnblock"],
    "Currency": ["GBP"],
    "MinimumValueDispensed": "£5",
    "distance": 263.8181727485233
}, {
    "_id": "59baea9cb8b7a8b83a14a737",
    "Organisation": {
        "ParentOrganisation": {
            "LEI": "549300PPXHEU2JF0AM85",
            "OrganisationName": {
                "LegalName": "LLOYDS BANKING GROUP PLC"
            }
        },
        "Brand": {
            "TrademarkIPOCode": "UK",
            "TrademarkID": "UK00001286876"
        }
    },
    "BranchIdentification": "77730199",
    "ATMID": "L838BC11",
    "LocationCategory": "FactoryOrOffice",
    "SiteID": "77730101",
    "Address": {
        "StreetName": "CANONS HOUSE",
        "BuildingNumberOrName": "REMOTE - OTHER",
        "PostCode": "BS1 5LL",
        "OptionalAddressField": "CANONS HOUSE; CANONS WAY",
        "TownName": "BRISTOL",
        "CountrySubDivision": "AVON",
        "Country": "GB"
    },
    "GeographicLocation": {
        "Latitude": "51.448621",
        "Longitude": "-2.60085"
    },
    "AccessibilityTypes": ["AudioCashMachine", "WheelchairAccess"],
    "SupportedLanguages": ["eng", "spa", "ger", "fre"],
    "ATMServices": ["CashWithdrawal", "PINChange", "MobilePhoneTopUp", "Balance", "FastCash"],
    "AdditionalATMServices": ["PINUnblock"],
    "Currency": ["GBP"],
    "MinimumValueDispensed": "£5",
    "distance": 263.8181727485233
}, {
    "_id": "59baeab0b8b7a8b83a14c05b",
    "Organisation": {
        "ParentOrganisation": {
            "LEI": "PTCQB104N23FMNK2RZ28",
            "BIC": "ABBYGB2LXXX",
            "OrganisationName": {
                "LegalName": "Santander UK PLC"
            }
        },
        "Brand": {
            "TrademarkIPOCode": "EU",
            "TrademarkID": "9415605"
        }
    },
    "BranchIdentification": "6381",
    "ATMID": "A015837A",
    "SiteID": "1158",
    "SiteName": "Bristol BS",
    "Address": {
        "StreetName": "12 Baldwin Street",
        "BuildingNumberOrName": "12",
        "PostCode": "BS1 1SD",
        "TownName": "Bristol",
        "CountrySubDivision": "Avon",
        "Country": "GB"
    },
    "GeographicLocation": {
        "Latitude": "51.45324672",
        "Longitude": "-2.59630589"
    },
    "SupportedLanguages": ["English", "Spanish"],
    "ATMServices": ["CashWithdrawal", "CashDeposits", "ChequeDeposits"],
    "Currency": ["£ GBP"],
    "MinimumValueDispensed": "£10",
    "distance": 706.267179309197
}
]
```

2. `GET /api/all-atms`
    - `limit` (optional) Number limit number of results to limit to

Gets **ALL** ATMS in full Open Banking API ATM form, truncated by limit if provided.

3. `GET /api/bank-names`

All bank names and how many ATMs they have.

```json
{
    "Bank of Ireland (UK) plc": {
        "BankName": "Bank of Ireland (UK) plc",
        "ATMCount": 199
    },
    "Barclays Bank PLC": {
        "BankName": "Barclays Bank PLC",
        "ATMCount": 2131
    },
    "Danske Bank": {
        "BankName": "Danske Bank",
        "ATMCount": 182
    },
    "AIB Group (UK) p.l.c.": {
        "BankName": "AIB Group (UK) p.l.c.",
        "ATMCount": 87
    },
    "LLOYDS BANKING GROUP PLC": {
        "BankName": "LLOYDS BANKING GROUP PLC",
        "ATMCount": 4440
    },
    "HSBCG": {
        "BankName": "HSBCG",
        "ATMCount": 1827
    },
    "Nationwide Building Society": {
        "BankName": "Nationwide Building Society",
        "ATMCount": 1378
    },
    "Royal Bank of Scotland Group plc": {
        "BankName": "Royal Bank of Scotland Group plc",
        "ATMCount": 7361
    },
    "Santander UK PLC": {
        "BankName": "Santander UK PLC",
        "ATMCount": 2357
    }
}
```

3. `GET /api/bank-facilities`

Bank facilities and how many ATMs have them

```json
{
    "CashWithdrawal": {
        "FacilityName": "CashWithdrawal",
        "ATMCount": 39924
    },
    "Balance": {
        "FacilityName": "Balance",
        "ATMCount": 35210
    },
    "PINChange": {
        "FacilityName": "PINChange",
        "ATMCount": 35210
    },
    "PINActivation": {
        "FacilityName": "PINActivation",
        "ATMCount": 18240
    },
    "BillPayments": {
        "FacilityName": "BillPayments",
        "ATMCount": 3194
    },
    "CashDeposits": {
        "FacilityName": "CashDeposits",
        "ATMCount": 5844
    },
    "MiniStatement": {
        "FacilityName": "MiniStatement",
        "ATMCount": 25568
    },
    "MobilePaymentRegistration": {
        "FacilityName": "MobilePaymentRegistration",
        "ATMCount": 4262
    },
    "MobilePhoneTopUp": {
        "FacilityName": "MobilePhoneTopUp",
        "ATMCount": 34442
    },
    "OrderStatement": {
        "FacilityName": "OrderStatement",
        "ATMCount": 174
    },
    "FastCash": {
        "FacilityName": "FastCash",
        "ATMCount": 8880
    },
    "CharityDonation": {
        "FacilityName": "CharityDonation",
        "ATMCount": 3654
    },
    "ChequeDeposits": {
        "FacilityName": "ChequeDeposits",
        "ATMCount": 5736
    }
}
```

## TODOs / Bug List / Enhancements
* Pagination of results using start and limit. Not too familiar with generators to only yield parts of the list from the endpoint.
* Filtering by ~~facilities e.g. bill payments~~ (done) or ~~a particular bank (e.g. free withdrawals only from their home bank).~~ (also done)
* Adding bank logos as the map markers.
* ~~More info in the map infowindow. The attribute `SiteName` is usually not populated so it's hard to name the ATMs unless the full address (combining the split address parts in order) is used.~~
* ~~Overlapping results. There may be a few ATMs in the same location. Presently the markers all overlap and it's impossible to tell that there are multiple ATMs at a given marker.~~

## Sources
* [Introduction to Generators & Koa.js: Part 2](https://code.tutsplus.com/tutorials/introduction-to-generators-koajs-part-2--cms-21756)
* [Monk NPM](https://automattic.github.io/monk/)
* [Open Banking APIs](https://www.openbanking.org.uk/developers/api-dashboard/)
* [Latitude longitude calculation](http://www.movable-type.co.uk/scripts/latlong.html)