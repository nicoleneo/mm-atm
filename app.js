var koa = require('koa');
var app = koa();
var router = require('koa-router');
var mount = require('koa-mount');
var api = require('./api/api.js');
var send = require('koa-send');
var serve = require('koa-static');

var APIv1 = new router();
APIv1.get('/all-atms', api.all);
APIv1.get('/bank-names', api.bankNames);
APIv1.get('/bank-facilities', api.bankFacilities);
APIv1.get('/nearest-atms', api.nearestATMs);

app.use(mount('/api', APIv1.middleware()));

app.use(serve(__dirname + '/public'));

if (!module.parent) app.listen(3000);
console.log('ATM Finder is Running on http://localhost:3000/');